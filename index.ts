/*The index.ts file configures the express server 
and specifies the available endpoints*/

import express from 'express';
import { Account } from './src/entities/account';
import { AccountType } from './src/entities/account-type';
import { Client } from './src/entities/client';
import { MissingResourceError } from './src/errors';
import AccountService from './src/services/account-service';
import { AccountServiceImpl } from './src/services/account-service-impl';
import AccountTypeService from './src/services/account-type-service';
import { AccountTypeServiceImpl } from './src/services/account-type-service-impl';
import ClientService from './src/services/client-service';
import { ClientServiceImple } from './src/services/client-service-impl';

const port = 3001;
//create an instance of express
const app = express();
app.use(express.json()); //middleware

//Create a clientServiceImpl object
const clientService:ClientService = new ClientServiceImple();

//Creat an accountTypeServiceImpl object
const accountTypeService:AccountTypeService = new AccountTypeServiceImpl();

//Creat an accountTypeServiceImpl object
const accountService:AccountService = new AccountServiceImpl();

//Client realted http enpoints here
app.post("/clients", async (req, res) => {
    let client:Client = req.body;
    client = await clientService.addClient(client);
    res.status(201);
    res.send(client);
});

app.get("/clients", async (req, res) => {
    console.log("hit clients");
    const clients:Client[] = await clientService.getAllClients();
    res.status(200);
    res.send(clients);
});

app.get("/clients/:id", async (req, res) => {
    try {
        console.log("hit get clients by Id");
        const clientId = Number(req.params.id);
        console.log(clientId);
        const client:Client = await clientService.getClient(clientId);
        res.status(200);
        res.send(client);
    } catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

app.put("/clients/:id", async (req, res) => {
    try {
        console.log("hit update client by Id");
        const clientId = Number(req.params.id);
        console.log(clientId);
        const client:Client = await clientService.getClient(clientId);
        console.log(client);

        let updatedClient:Client = req.body;
        console.log(updatedClient.fname);
        console.log(updatedClient.lname);
        console.log(updatedClient.id);

        updatedClient.id = client.id;
        console.log(updatedClient);

        updatedClient = await clientService.editClient(updatedClient);
        
        res.status(200);
        res.send(updatedClient);
    } catch (error) {
        if(error instanceof MissingResourceError){
        res.status(404);
        res.send(error);
        }
    }
});

app.delete("/clients/:id", async (req, res) => {
    try {
        console.log("hit delete client by Id");
        const clientId = Number(req.params.id);
        console.log(clientId);
        const response = await clientService.deleteClient(clientId);
        res.status(205);
        res.send(response);
    } catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

//Account type related http enpoints

app.post("/account-types", async (req, res) => {
    console.log("hit post account type");
    let accountType:AccountType = req.body;
    accountType = await accountTypeService.addAccountType(accountType);
    res.status(201);
    res.send(accountType);
});

app.get("/account-types", async (req, res) => {
    console.log("hit get all account types");
    const accountTypes:AccountType[] = await accountTypeService.getAllAccountTypes();
    res.status(200);
    res.send(accountTypes);
});

app.get("/account-types/:id", async (req, res) => {
    try {
        console.log("hit get account types by Id");
        const accountTypeId = Number(req.params.id);
        console.log(accountTypeId);
        const accountType:AccountType = await accountTypeService.getAccountType(accountTypeId);
        res.status(200);
        res.send(accountType);
    } catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

app.put("/account-types/:id", async (req, res) => {
    try {
        console.log("hit update accounttypes by Id");
        const accountTypeId = Number(req.params.id);
        console.log(accountTypeId);
        const accountType:AccountType = await accountTypeService.getAccountType(accountTypeId);
        console.log(accountType);

        let updatedAccountType:AccountType = req.body;
        console.log(updatedAccountType.type);
        console.log(updatedAccountType.id);
    

        updatedAccountType.id = accountType.id;
        console.log(updatedAccountType);

        updatedAccountType = await accountTypeService.editAccountType(updatedAccountType);
        
        res.status(200);
        res.send(updatedAccountType);
    } catch (error) {
        if(error instanceof MissingResourceError){
        res.status(404);
        res.send(error);
        }
    }
});

app.delete("/account-types/:id", async (req, res) => {
    try {
        console.log("hit delete account-type by Id");
        const accountTypeId = Number(req.params.id);
        console.log(accountTypeId);
        const response = await accountTypeService.deleteAccountType(accountTypeId);
        res.status(205);
        res.send(response);
    } catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

//Account related http enpoints

app.post("/clients/:id/accounts", async (req, res) => {
    console.log("hit post account");
    const clientId = Number(req.params.id);
    let account:Account = req.body;
    account.clientId = clientId;
    account = await accountService.addAccount(account);
    res.status(201);
    res.send(account);
});

app.get("/clients/:id/accounts", async (req, res) => {
    try {
        console.log("hit get all accounts by client id");
        const clientId = Number(req.params.id);
        const accounts:Account[] = await accountService.getAllAccountsByClientId(clientId);
        res.status(200);
        res.send(accounts);
    } catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

//How to get query parameters
//https://stackabuse.com/get-query-strings-and-parameters-in-express-js/
app.get("/accounts", async (req, res) => {
    
    if(!req.query.amountLessThan){
        console.log("hit get all accounts");
        console.log(req.query);
        console.log(!req.query);
        console.log(req.query.amountLessThan);
        const accounts:Account[] = await accountService.getAllAccounts();
        res.status(200);
        res.send(accounts);
    }else{
        console.log("hit query parames get all");
        console.log(req.query);
        console.log(!req.query);
        const to:number = Number(req.query.amountLessThan);
        const from:number = Number(req.query.amountGreaterThan);
        const filteredAccounts:Account[] = await accountService.getAllAccountsWithBalanceInRange(from, to);
        res.status(200);
        res.send(filteredAccounts);
    }

});

app.get("/accounts/:id", async (req, res) => {
    try {
        console.log("hit get accounts by Id");
        const accountId = Number(req.params.id);
        console.log(accountId);
        const account:Account = await accountService.getAccount(accountId);
        res.status(200);
        res.send(account);
    } catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

app.put("/accounts/:id", async (req, res) => {
    try {
        console.log("hit update account by Id");
        const accountId = Number(req.params.id);
        console.log(accountId);
        const account:Account = await accountService.getAccount(accountId);
        console.log(account);

        let updatedAccount:Account = req.body;
        console.log(updatedAccount.balance);
        console.log(updatedAccount.clientId);
        console.log(updatedAccount.typeId);
        console.log(updatedAccount.id);
    

        updatedAccount.id = account.id;
        console.log(updatedAccount);

        updatedAccount = await accountService.editAccount(updatedAccount);
        
        res.status(200);
        res.send(updatedAccount);
    } catch (error) {
        if(error instanceof MissingResourceError){
        res.status(404);
        res.send(error);
        }
    }
});

app.delete("/accounts/:id", async (req, res) => {
    try {
        console.log("hit delete accounts by Id");
        const accountId = Number(req.params.id);
        console.log(accountId);
        const response = await accountService.deleteAccount(accountId);
        res.status(205);
        res.send(response);
    } catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

app.patch("/accounts/:id/deposit", async (req, res) => {
    try {
        const updatedAccount:Account = await accountService.deposit(Number(req.params.id), Number(req.body.amount));
        console.log("deposit updated account")
        console.log(!updatedAccount);
        console.log(Boolean(updatedAccount));
        if (Boolean(updatedAccount)){
            res.status(202);
            res.send(updatedAccount);
        }
    } catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

app.patch("/accounts/:id/withdraw", async (req, res) => {
    try {
        const updatedAccount:Account = await accountService.withdraw(Number(req.params.id), Number(req.body.amount));
        if ((Boolean(updatedAccount))){
            res.status(202);
            res.send(updatedAccount);
        }
    } catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});


//Start the server at given port
app.listen(port, ()=>{console.log(`Bank API Application Started on port ${port}`)});