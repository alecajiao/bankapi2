export class Account {
    constructor (
        public id:number,
        public balance:number,
        public clientId:number,
        public typeId:number
    ) {}
}