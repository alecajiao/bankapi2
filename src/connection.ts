require('dotenv').config({path:'/Users/gnz11/Documents/revature/bankApi/app.env'})
import {Client} from 'pg';

export const dbClient = new Client({
    user:'postgres',
    password:process.env.DBPW, // should never store passwords on code
    database:process.env.DBNAME,
    port:5432,
    host: '35.231.148.127'
});

dbClient.connect();

// Calling client.conenct() => without a callback yields a promise 
//client.connect() => Promise<void>
//https://node-postgres.com/api/client
// dbClient
//   .connect()
//   .then(() => console.log('connected'))
//   .catch(err => console.error('connection error', err.stack))