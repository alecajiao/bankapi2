import { dbClient } from "../../connection";
import { AccountType } from "../../entities/account-type";
import { MissingResourceError } from "../../errors";
import { AccountTypeDAO } from "../dao-interfaces/account-type-dao";

export class AccountTypeDAOPostgres implements AccountTypeDAO{
    async createAccountType(accountType: AccountType): Promise<AccountType> {
        const sql:string = "insert into bankdb.account_type (type) values ($1) returning id";
        const values = [accountType.type];
        const result = await dbClient.query(sql, values);  
        accountType.id = result.rows[0].id;
        return accountType;
    }

    async getAllAccountTypes(): Promise<AccountType[]> {
        const sql:string = "select * from bankdb.account_type";
        const result = await dbClient.query(sql);
        const accountTypes:AccountType[] = [];
        for(const row of result.rows){
            const accountType:AccountType = new AccountType(
                row.id,
                row.type
            );
            accountTypes.push(accountType);
        }
        return accountTypes;
    }

    async getAccountTypeById(id: number): Promise<AccountType> {
        const sql:string = "select * from bankdb.account_type where id = $1";
        const values = [id];
        const result = await dbClient.query(sql, values);
        const row = result.rows[0];

        if(result.rowCount === 0){
            throw new MissingResourceError(`The account type  with id ${id} does not exist`);
        }

        const accountType:AccountType = new AccountType(
            row.id,
            row.type
        );

        return accountType;
    }

    async updateAccountType(accountType: AccountType): Promise<AccountType> {
        const sql:string = 'update bankdb.account_type set type = $1 where id = $2';
        const values = [accountType.type, accountType.id]; 
        const result = await dbClient.query(sql, values);
        console.log(result);

        if(result.rowCount === 0){
            throw new MissingResourceError(`The account type with id ${accountType.id} does not exist`);
        }

        return accountType;
    }

    async deleteAccountTypeById(id: number): Promise<boolean> {
        const sql:string = 'delete from bankdb.account_type where id = $1';
        const values = [id];
        const result = await dbClient.query(sql, values);

        if(result.rowCount === 0){
            throw new MissingResourceError(`The account type with id ${id} does not exist`);
        }
        return true;
    }
}