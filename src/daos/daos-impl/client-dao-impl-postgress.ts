import { Client } from "../../entities/client";
import { ClientDAO } from "../dao-interfaces/client-dao";
import { dbClient } from "../../connection";
import { MissingResourceError } from "../../errors";

export class ClientDAOPostgres implements ClientDAO{
    async createClient(client: Client): Promise<Client> {
        const sql:string = "insert into bankdb.client (f_name, l_name) values ($1, $2) returning id";
        const values = [client.fname, client.lname];
        const result = await dbClient.query(sql, values);  
        client.id = result.rows[0].id;
        return client;
    }

    async getAllClients(): Promise<Client[]> {
        const sql:string = "select * from bankdb.client";
        const result = await dbClient.query(sql);
        const clients:Client[] = [];
        for(const row of result.rows){
            const client:Client = new Client(
                row.id,
                row.f_name,
                row.l_name
            );
            clients.push(client);
        }
        return clients;
    }

    async getClientById(id: number): Promise<Client> {
        const sql:string = "select * from bankdb.client where id = $1";
        const values = [id];
        const result = await dbClient.query(sql, values);
        const row = result.rows[0];

        if(result.rowCount === 0){
            throw new MissingResourceError(`The client with id ${id} does not exist`);
        }

        const client:Client = new Client(
            row.id,
            row.f_name,
            row.l_name
        );

        return client;
    }
    async updateClient(client: Client): Promise<Client> {
        const sql:string = 'update bankdb.client set f_name = $1, l_name = $2 where id = $3';
        const values = [client.fname, client.lname, client.id]; 
        const result = await dbClient.query(sql, values);

        if(result.rowCount === 0){
            throw new MissingResourceError(`The client with id ${client.id} does not exist`);
        }

        return client;
    }

    async deleteClientById(id: number): Promise<boolean> {
        const sql:string = 'delete from bankdb.client where id = $1';
        const values = [id];
        const result = await dbClient.query(sql, values);

        if(result.rowCount === 0){
            throw new MissingResourceError(`The client with id ${id} does not exist`);
        }
        return true;
    }
}