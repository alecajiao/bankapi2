import { dbClient } from "../../connection";
import { Account } from "../../entities/account";
import { MissingResourceError } from "../../errors";
import { AccountDAO } from "../dao-interfaces/account-dao";

export class AccountDAOPostgres implements AccountDAO{
    async createAccount(account:Account): Promise<Account> {
        const sql:string = "insert into bankdb.account (balance, client_id, type_id) values ($1, $2, $3) returning id";
        const values = [account.balance, account.clientId, account.typeId];
        const result = await dbClient.query(sql, values);
        account.id = result.rows[0].id;
        return account;
    }

    async getAllAccounts(): Promise<Account[]> {
        const sql:string = "select * from bankdb.account";
        const result = await dbClient.query(sql);
        const accounts:Account[] = [];
        for(const row of result.rows){
            const account:Account = new Account(
                row.id,
                row.balance,
                row.client_id,
                row.type_id
            );
            accounts.push(account);
        };

        return accounts;
    }

    async getAccountById(id: number): Promise<Account> {
        const sql:string = "select * from bankdb.account where id = $1";
        const values = [id];
        const result = await dbClient.query(sql, values);
        const row = result.rows[0];

        if(result.rowCount === 0){
            throw new MissingResourceError(`The account with id ${id} does not exist`);
        }

        const account:Account = new Account(
            row.id,
            row.balance,
            row.client_id,
            row.type_id
        );

        return account;
    }
    async updateAccount(account: Account): Promise<Account> {
        const sql:string = 'update bankdb.account set balance = $1, client_id = $2, type_id = $3 where id = $4';
        const values = [account.balance, account.clientId, account.typeId, account.id]; 
        const result = await dbClient.query(sql, values);

        if(result.rowCount === 0){
            throw new MissingResourceError(`The account with id ${account.id} does not exist`);
        }

        return account;
    }

    async deleteAccountById(id: number): Promise<boolean> {
        const sql:string = 'delete from bankdb.account where id = $1';
        const values = [id];
        const result = await dbClient.query(sql, values);

        if(result.rowCount === 0){
            throw new MissingResourceError(`The account with id ${id} does not exist`);
        }
        return true;
    }
}