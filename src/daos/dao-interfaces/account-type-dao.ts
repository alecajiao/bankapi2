import { AccountType } from "../../entities/account-type";

export interface AccountTypeDAO{
    //CREATE
    createAccountType(accountType:AccountType):Promise<AccountType>;

    //READ
    getAllAccountTypes():Promise<AccountType[]>;
    getAccountTypeById(id:number):Promise<AccountType>;

    //UPDATE
    updateAccountType(accountType:AccountType):Promise<AccountType>;

    //DELETE
    deleteAccountTypeById(id:number):Promise<boolean>;
}