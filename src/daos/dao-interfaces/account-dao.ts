import { Account } from "../../entities/account";

export interface AccountDAO{
    //CREATE
    createAccount(account:Account):Promise<Account>;

    //READ
    getAllAccounts():Promise<Account[]>;
    getAccountById(id:number):Promise<Account>;

    //UPDATE
    updateAccount(account:Account):Promise<Account>;

    //DELETE
    deleteAccountById(id:number):Promise<boolean>;
}