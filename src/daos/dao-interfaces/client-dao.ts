import { Client } from "../../entities/client";

export interface ClientDAO{
    //CREATE
    createClient(client:Client):Promise<Client>;

    //READ
    getAllClients():Promise<Client[]>;
    getClientById(id:number):Promise<Client>;

    //UPDATE
    updateClient(client:Client):Promise<Client>;

    //DELETE
    deleteClientById(id:number):Promise<boolean>;
}