import { AccountType } from "../entities/account-type";

export default interface AccountTypeService{
    //Baisc CRUD operations
    addAccountType(accountType:AccountType):Promise<AccountType>;
    getAllAccountTypes():Promise<AccountType[]>;
    getAccountType(id:number):Promise<AccountType>;
    editAccountType(accountType:AccountType):Promise<AccountType>;
    deleteAccountType(id:number):Promise<boolean>;
}