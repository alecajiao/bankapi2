import { dbClient } from "../connection";
import { AccountDAO } from "../daos/dao-interfaces/account-dao";
import { AccountDAOPostgres } from "../daos/daos-impl/account-dao-impl-postgress";
import { Account } from "../entities/account";
import { MissingResourceError } from "../errors";
import AccountService from "./account-service";

export class AccountServiceImpl implements AccountService{
    accountDAO:AccountDAO = new AccountDAOPostgres();

    addAccount(account: Account): Promise<Account> {
        return this.accountDAO.createAccount(account);
    }

    getAllAccounts(): Promise<Account[]> {
        return this.accountDAO.getAllAccounts();
    }

    getAccount(id: number): Promise<Account> {
        return this.accountDAO.getAccountById(id);
    }

    editAccount(account:Account): Promise<Account> {
        return this.accountDAO.updateAccount(account);
    }

    deleteAccount(id: number): Promise<boolean> {
        return this.accountDAO.deleteAccountById(id);
    }

    async deposit(accountId: number, amount: number): Promise<Account> {
        let account:Account = await this.getAccount(accountId);

        console.log("from deposit");
        console.log(account)
        console.log(!account);
        console.log(!Boolean(account));
        if(!account){
            throw new MissingResourceError(`The account with id ${accountId} does not exist`);
        }
        console.log(typeof account.balance);
        console.log(typeof amount);
        account.balance = Number(account.balance) + amount;

        return this.accountDAO.updateAccount(account);
    }

    async withdraw(accountId: number, amount: number): Promise<Account> {
        let account:Account = await this.getAccount(accountId);

        if(!account){
            throw new MissingResourceError(`The account with id ${accountId} does not exist`);
        }

        account.balance = Number(account.balance) - amount;

        return this.accountDAO.updateAccount(account);
    }

    async getAllAccountsByClientId(id: number): Promise<Account[]> {
        const accounts:Account[] = await this.getAllAccounts();
        const clientAccounts = accounts.filter(account => account.clientId === id);
 
        if(accounts.length === 0 ){
            throw new MissingResourceError(`The client with id ${id} does not have any accounts`);
        }

        return clientAccounts;
    }
    
    async getAllAccountsWithBalanceInRange(from: number, to: number): Promise<Account[]> {
        const accounts:Account[] = await this.getAllAccounts();
        const clientAccounts = accounts.filter(account => account.balance >= from && account.balance <= to);

        return clientAccounts;
    }
  
}