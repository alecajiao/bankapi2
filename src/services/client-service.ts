import { Account } from "../entities/account";
import { Client } from "../entities/client";

export default interface ClientService{
    //Baisc CRUD operations
    addClient(client:Client):Promise<Client>;
    getAllClients():Promise<Client[]>;
    getClient(id:number):Promise<Client>;
    editClient(client:Client):Promise<Client>;
    deleteClient(id:number):Promise<boolean>;

    //Account/Client related operations
    // addAccount(clientId:number):Promise<Account>;
    // getAllAccounts(clientId:number):Promise<Account[]>;
    // deleteAccount(clientId:number, accountId:number):Promise<boolean>;
}