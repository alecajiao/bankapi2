import { Account } from "../entities/account";

export default interface AccountService{
    //Baisc CRUD operations
    addAccount(account:Account):Promise<Account>;
    getAllAccounts():Promise<Account[]>;
    getAccount(id:number):Promise<Account>;
    editAccount(account:Account):Promise<Account>;
    deleteAccount(id:number):Promise<boolean>;

    //PATCH
    deposit(accountId:number, amount:number):Promise<Account>;
    withdraw(accountId:number, amount:number):Promise<Account>;

    //Special GET
    getAllAccountsByClientId(id:number):Promise<Account[]>;
    getAllAccountsWithBalanceInRange(from:number, to:number):Promise<Account[]>;

}