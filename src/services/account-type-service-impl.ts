import { AccountTypeDAO } from "../daos/dao-interfaces/account-type-dao";
import { AccountTypeDAOPostgres } from "../daos/daos-impl/account-type-dao-impl-postgress";
import { AccountType } from "../entities/account-type";
import AccountTypeService from "./account-type-service";

export class AccountTypeServiceImpl implements AccountTypeService{
    accountTypeDAO:AccountTypeDAO = new AccountTypeDAOPostgres();

    addAccountType(accountType: AccountType): Promise<AccountType> {
        return this.accountTypeDAO.createAccountType(accountType);
    }
    getAllAccountTypes(): Promise<AccountType[]> {
        return this.accountTypeDAO.getAllAccountTypes();
    }
    getAccountType(id: number): Promise<AccountType> {
        return this.accountTypeDAO.getAccountTypeById(id);
    }
    editAccountType(accountType: AccountType): Promise<AccountType> {
        return this.accountTypeDAO.updateAccountType(accountType);
    }
    deleteAccountType(id: number): Promise<boolean> {
        return this.accountTypeDAO.deleteAccountTypeById(id);
    }
}