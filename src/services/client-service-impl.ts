import { ClientDAO } from "../daos/dao-interfaces/client-dao";
import { ClientDAOPostgres } from "../daos/daos-impl/client-dao-impl-postgress";
import { Client } from "../entities/client";
import ClientService from "./client-service";

export class ClientServiceImple implements ClientService{
    clientDAO:ClientDAO = new ClientDAOPostgres();
    
    addClient(client: Client): Promise<Client> {
        return this.clientDAO.createClient(client);
    }

    getAllClients(): Promise<Client[]> {
        return this.clientDAO.getAllClients();
    }

    getClient(id: number): Promise<Client> {
        return this.clientDAO.getClientById(id);
    }

    editClient(client: Client): Promise<Client> {
        return this.clientDAO.updateClient(client);
    }
    
    deleteClient(id: number): Promise<boolean> {
        return this.clientDAO.deleteClientById(id);
    }
}