--Adding accounts to seeded clients
insert into bankdb.client (id, f_name, l_name) values
    (default, 'Hermione', 'Granger'),
    (default, 'Ron', 'Weasley'),
    (default, 'Luna', 'Lovegood'),
    (default, 'Harry', 'Potter'),
    (default, 'Remus', 'Lupin'),
    (default, 'Albus', 'Dumbledore');

insert into bankdb.account_type (id, type) values
    (default, 'checking'),
    (default, 'savings');



