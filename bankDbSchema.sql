create schema bankdb
    -- serial notational convenience
    -- equivalent to: integer not null default {sequence}
    --https://www.postgresql.org/docs/9.3/datatype-numeric.html#DATATYPE-SERIAL
    -- The primary key constraint is the combination of not null and unique constraints
    -- https://www.w3resource.com/PostgreSQL/primary-key-constraint.php
    create table if not exists client (
        id serial primary key,
        f_name varchar(200) not null,
        l_name varchar(200) not null
        )
    create table if not exists account_type (
        id serial primary key,
        type varchar(200) not null unique
        )
    create table if not exists account (
        --Numeric type
        --https://www.postgresql.org/docs/13/datatype-numeric.html
        --also
        --Recommended to convert money to cents and store as int
        id serial primary key,
        balance numeric not null default 0,
        client_id int not null references client(id),
        type_id int not null references account_type(id)
        );