import { dbClient } from "../src/connection";
import { AccountTypeDAO } from "../src/daos/dao-interfaces/account-type-dao";
import { AccountTypeDAOPostgres } from "../src/daos/daos-impl/account-type-dao-impl-postgress";
import { AccountType } from "../src/entities/account-type";

const accountTypeDAO:AccountTypeDAO = new AccountTypeDAOPostgres();

var accountTypeCounter = Math.random();

test("Create an Account Type", async ()=>{
    const testAccountType:AccountType = new AccountType(0, `Test type ${accountTypeCounter}`);
    accountTypeCounter++;
    const result:AccountType = await accountTypeDAO.createAccountType(testAccountType);
    expect(result.id).not.toBe(0);// An entity that is saved should have non-zero id 
});

test("Get Account Type by Id", async ()=>{
    let testAccountType:AccountType = new AccountType(0, `Test type ${accountTypeCounter}`);
    accountTypeCounter++;
    testAccountType = await accountTypeDAO.createAccountType(testAccountType);

    let retrievedAccountType:AccountType = await accountTypeDAO.getAccountTypeById(testAccountType.id);

    expect(retrievedAccountType.id).toBe(testAccountType.id);
    expect(retrievedAccountType.type).toBe(testAccountType.type);
});

test("Get all Account Types", async()=>{
    const accountTypes:AccountType[] = await accountTypeDAO.getAllAccountTypes();

    const accountType:AccountType = new AccountType(0, `Test type ${accountTypeCounter}`);
    accountTypeCounter++;
    await accountTypeDAO.createAccountType(accountType);

    const accountTypesUpdated:AccountType[] = await accountTypeDAO.getAllAccountTypes();
    
    expect(accountTypesUpdated.length).toBeGreaterThanOrEqual(accountTypes.length);
});

test("Update Account Type", async ()=> {
    let accountType:AccountType = new AccountType(0, `Test type ${accountTypeCounter}`);    
    accountType = await accountTypeDAO.createAccountType(accountType);

    accountType.type = `Test type ${accountTypeCounter} updated`;
      
    accountType = await accountTypeDAO.updateAccountType(accountType);

    expect(accountType.type).toBe(`Test type ${accountTypeCounter} updated`);
    accountTypeCounter++;
});

test("delete Account Type by Id", async ()=>{
    let accountType:AccountType = new AccountType(0, `Test type ${accountTypeCounter}`);  
    accountTypeCounter++;
    accountType = await accountTypeDAO.createAccountType(accountType);

    const result:boolean = await accountTypeDAO.deleteAccountTypeById(accountType.id);
    // expect(result).toBeTruthy();
    expect(result).toEqual(true);
});


afterAll(async () => {
    dbClient.end(); // close connection
});