import { dbClient } from "../src/connection";
import { AccountDAO } from "../src/daos/dao-interfaces/account-dao";
import { AccountDAOPostgres } from "../src/daos/daos-impl/account-dao-impl-postgress";
import { Account } from "../src/entities/account";

const accountDAO:AccountDAO = new AccountDAOPostgres();

test("Create an Account", async () => {
    const account:Account = await accountDAO.createAccount(new Account(0, 0, 1, 1));
    expect(account.id).not.toBe(0);
    expect(account.balance).toBe(0);
});

test("Get all Accounts", async () => {
    const accounts:Account[] = await accountDAO.getAllAccounts();
    await accountDAO.createAccount(new Account(0, 0, 1, 1));
    expect(accounts.length).toBeLessThan(accounts.length + 1);
});

test("Get Account by ID", async () => {
    const account:Account = await accountDAO.createAccount(new Account(0, 0, 1, 1));
    const retrievedAccount:Account = await accountDAO.getAccountById(account.id);
    expect(account.id).toEqual(retrievedAccount.id);
});

test("Update Account", async () => {
    const account:Account = await accountDAO.createAccount(new Account(0, 0, 1, 1));
    account.balance = 5000;
    const updatedAccount:Account = await accountDAO.updateAccount(account);
    expect(updatedAccount.balance).toBe(5000);

});

test("Delete Account", async () => {
    const account:Account = await accountDAO.createAccount(new Account(0, 0, 1, 1));
    const res1 = await accountDAO.deleteAccountById(account.id);
    expect(res1).toBe(true);
});

afterAll(async () => {
    dbClient.end(); // close connection
});