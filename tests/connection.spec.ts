import {dbClient} from '../src/connection';

// client is the main obejct we will use to make queries

test("Should create a connection", async () => {
    const result = await dbClient.query("select * from bankdb.client");
    console.log(result);
});

afterAll(()=>{dbClient.end();});