import { dbClient } from "../src/connection";
import { Client } from "../src/entities/client";
import { ClientDAO } from "../src/daos/dao-interfaces/client-dao";
import { ClientDAOPostgres } from "../src/daos/daos-impl/client-dao-impl-postgress";

const clientDAO:ClientDAO = new ClientDAOPostgres();

test("Create a client", async ()=>{
    const testClient:Client = new Client(0, "Alejandra", "Cajiao");
    const result:Client = await clientDAO.createClient(testClient);
    expect(result.id).not.toBe(0);// An entity that is saved should have non-zero id 
});


test("Get client by Id", async ()=>{
    let testClient:Client = new Client(0, "Bambi", "Cajiao");
    testClient = await clientDAO.createClient(testClient);

    let retrievedClient:Client = await clientDAO.getClientById(testClient.id);

    expect(retrievedClient.id).toBe(testClient.id);
    expect(retrievedClient.fname).toBe(testClient.fname);
    expect(retrievedClient.lname).toBe(testClient.lname);
});

test("Get all clients", async()=>{
    const client1:Client = new Client(0, "Rose", "Smith");
    const client2:Client = new Client(0, "Mary", "Blacksmith");

    await clientDAO.createClient(client1);
    await clientDAO.createClient(client2);

    const clients:Client[] = await clientDAO.getAllClients();
    expect(clients.length).toBeGreaterThanOrEqual(2);
});

test("Update Client", async ()=> {
    let client:Client = new Client(0, "Roxi", "Cajiao");    
    client = await clientDAO.createClient(client);

    client.lname = "Paredes";
    client = await clientDAO.updateClient(client);

    expect(client.lname).toBe("Paredes");
});

test("delete client by id", async ()=>{
    let client:Client = new Client(0, "Sam", "Cajiao");
    client = await clientDAO.createClient(client);

    const result:boolean = await clientDAO.deleteClientById(client.id);
    // expect(result).toBeTruthy();
    expect(result).toEqual(true);
});

afterAll(async () => {
    dbClient.end(); // close connection
});